import CalendarUtil from "../../utils/CalendarUtil";
function compareTime(a,b) {
    if (a.time < b.time)
        return -1;
    if (a.time > b.time)
        return 1;
    return 0;
}
const schedule = (state = [], action) => {
	switch (action.type) {
		case 'LOAD_SCHEDULE_TYPES':
			return {...state, types: Object.values(action.data), activeType:Object.values(action.data)[0].id };
        case 'LOAD_SCHEDULE':
            let schedule = action.data.sort(compareTime);
            let schedule_data = [];
            let timetable_hours = schedule[0]['time']!=='' ? CalendarUtil.getHours(parseInt(schedule[0]['time'].substring(0, 2)), parseInt(schedule[schedule.length-1]['time'].substring(0, 2))) : CalendarUtil.getHours(parseInt(schedule[1]['time'].substring(0, 2)), parseInt(schedule[schedule.length-1]['time'].substring(0, 2)));
		    let hour_starts = CalendarUtil.getHourStarts(timetable_hours);
            action.data.forEach(function(item){
		        let index = hour_starts.indexOf(item.time.substring(0, 2));
		        if(typeof schedule_data[index] === "undefined"){
                    schedule_data[index] = Array();
                }
		        if(item['time'][3]==='3'){
		        	item['half'] = true;
				}
		        schedule_data[index].push(item);
            });
            // console.log(schedule[0]);
            // console.log(schedule_data);
		    let trainers = [];
		    let classes = [];
		    schedule_data.forEach(function (element) {
				element.forEach(function (cell) {
					if(cell.trainers){
						cell.trainers.forEach(function (trainer) {
                            var found = false;
                            for(var i = 0; i < trainers.length; i++) {
                                if (trainers[i].id === trainer.id) {
                                    found = true;
                                    break;
                                }
                            }
                            if(!found){
                                trainers.push({id: trainer.id, name:trainer.name});
							}
                        });
					}
                    var found_class = false;
                    for(var i = 0; i < classes.length; i++) {
                        if (classes[i].title == cell.title) {
                            found_class = true;
                            break;
                        }
                    }
                    if(!found_class){
                        classes.push({id:cell.id, title:cell.title}
                        );
                    }

                });
                trainers.sort(function(a,b) {return (a.name > b.name) ? 1 : ((b.name > a.name ) ? -1 : 0);} );
                classes.sort(function(a,b) {return (a.title > b.title) ? 1 : ((b.title > a.title ) ? -1 : 0);} );
            });
		    // console.log(schedule_data);
			return {...state, data:schedule_data, activeType: action.id, classes: '', trainers: '',  class_options:classes, trainer_options: trainers, times:timetable_hours};
		case 'FILTER_SCHEDULE':
		    localStorage.setItem('trainers',action.trainers);
		    localStorage.setItem('classes', action.classes);
			let full_sched = state.data;
			if(action.trainers === '' && action.classes === ''){
				// console.log(state.full_schedule);
                full_sched.forEach(function (col) {
                    col.forEach(function (cell) {
                            cell['hide'] = false;
                    }
					)});
                return {...state, trainers:action.trainers, classes:action.classes, data:full_sched};
			}else{
				let sched = state.data;
				sched.forEach(function (col) {
					col.forEach(function (cell) {
                        let contains_trainer = false;
                        if(typeof cell.trainers!="undefined"){
                            cell.trainers.forEach(function (trainer) {
                                if(trainer.id == action.trainers){
                                    contains_trainer = true;
                                }
                            });
                        }
                        let hide = !((contains_trainer || action.trainers ==='') && (cell.title === action.classes || action.classes === ''));
                        cell['hide'] = hide;
                    });

				});
                return {...state, trainers:action.trainers, classes:action.classes, data:sched};
			}
		case 'SWITCH_STATUS':
		    let dayData = state.data[action.index[0]];
        dayData[action.index[1]].status = !dayData[action.index[1]].status;
		let newState =
			[
				...state.data.slice(0, action.index[0]),
				dayData,
				...state.data.slice(action.index[0]+1)
			];
			return {...state,data:newState,};
		default:
			return state
	}
}

export default schedule;
