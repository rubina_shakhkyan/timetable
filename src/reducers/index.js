import {combineReducers} from 'redux';
import schedule from './schedule';


const mainReducer = combineReducers({
	schedule,
	});

export default mainReducer;
