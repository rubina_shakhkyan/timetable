import moment from 'moment';
import 'moment/locale/hy-am';
let lang = localStorage.getItem('language') ? localStorage.getItem('language') : 'hy';
if(lang === 'hy'){
    moment.locale('hy-am');
}else{
    moment.locale('en-gb');
}
export default class CalendarUtil{
    static getWeekDays(){
        var days = [];
        var begin = moment().startOf('isoWeek');
        for (var i=0; i<7; i++) {
            days.push(begin.format('ddd, MMMM D'));
            begin.add('d', 1);
        }
        return days;
    }
    static isToday(day){
        if(day === moment().format('ddd, MMMM D')){
            return true;
        }else{
            return false;
        }


    }

    static getHours(firstHour, lastHour){
        let hours = [];
        for(let i = firstHour; i<=lastHour; i++){
            if(i>=10){
                hours.push(i+':00');
            }else{
                hours.push('0'+i+':00');
            }

        }
        return hours;
    }

    static getHourStarts(times){
        let hoursStarts = [];
        times.forEach(function (time) {
            hoursStarts.push(time.substr(0,2));
        });

        return hoursStarts;
    }
}
