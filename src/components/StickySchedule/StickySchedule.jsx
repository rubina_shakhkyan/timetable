import React, {Component} from 'react';
import {StickyTable, Row, Cell} from 'react-sticky-table';
import 'react-sticky-table/dist/react-sticky-table.css';
import './StickySchedule.css';
import Time from "../Time";
import ScheduleItem, {NoDataItem} from './../ScheduleItem';
import CalendarUtil from "../../utils/CalendarUtil";

export default class Basic extends Component {
    render() {
        var rows = [];
        var cells = [];
        let self = this;
        let time_cells = [];
        if (typeof self.props.data !== "undefined") {
            time_cells.push(<Cell key={'000'}><div> </div></Cell>);
            {this.props.weekdays.map((value, c) => (
                time_cells.push(<Cell key={c} >
                    <div className= {`main-day ${ CalendarUtil.isToday(value) ? ('active') : ('')}` }>
                        {value}
                    </div>
                </Cell>)
            ))}
            rows.push(
                <Row className="calendar-days" key={'time_row_0'} >
                    {time_cells}
                </Row>
            );
            self.props.data.forEach(function (val, key) {
                cells = [];
                cells.push(
                    <Cell className="time-td" key={'time' + key}>
                        <Time time={self.props.times[key]}/>
                    </Cell>
                );
                var data = new Array(7);
                var col_data = typeof self.props.data[key] !== "undefined" ? self.props.data[key] : [];
                col_data.forEach(function (k, value) {
                    if(typeof data[parseInt(k.weekday-1)] === "undefined" ){
                        data[parseInt(k.weekday-1)]= [k];
                    }else{
                        data[parseInt(k.weekday-1)]= data[parseInt(k.weekday-1)].concat(k);
                    }

                });
                let fullInd = -1;
                // var rows = [];
                for(var i = 0; i < 7; i++){
                    if(data[i]){
                        fullInd++;
                        let _cell = [];
                            data[i].forEach(function (item,key) {
                                _cell.push(<ScheduleItem key={item.id} data = {item} switchStatus={self.props.switchStatus} index ={[self.props.index, fullInd+key]} activeType={self.props.activeType} />);

                            });

                            cells.push(<Cell className="day-main"  key={i}>{_cell}</Cell>);

                    }else{
                        cells.push(<Cell key={i}><NoDataItem/></Cell>);
                    }
                }

                rows.push(<Row key={key}>
                    {cells}
                </Row>);

            });
        }

        return (
            <div>
                <div className="sticky_table_wrapper">

                    {rows.length > 0 ? (
                    <StickyTable >
                        {rows}
                    </StickyTable>

                    ) : (null)}

                </div>
            </div>
        );
    }
}