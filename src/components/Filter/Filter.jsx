import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './Filter.css';
import { filterSchedule } from "../../actions";

class FilterComponent extends Component {
  render() {
      const lang = window.location.pathname.slice(-2) == 'en' || window.location.pathname.slice(-2) == 'hy' ? window.location.pathname.slice(-2) : 'hy';
      return (
      <form action="" className="schedule_filter">
        <div className="row">
          <div className="column small-12 medium-6 large-4">
            <div className="flex-container">
              <div className="filter_icon">
                <i className="icon icon-filter">
                </i>
              </div>
              <div className="input_group input_group-linear">
                <div className="select-wrapper">
                  <select name="" id=""
                          onChange={ (e) => {
                              this.props.filterSchedule(this.props.trainers, e.target.value)
                          } }>
                    <option value="">{lang ==='en' ? ('Classes') : ('Խմբային մարզումներ')}</option>
                      {this.props.class_options ? this.props.class_options.map((option) => (
                          <option value={option.title} key={option.id}>{option.title}</option>
                      )) :(null)}
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className="column small-12 medium-6 large-4">
            <div className="input_group input_group-linear">
              <div className="select-wrapper">
                  <select name="" id=""
                          onChange={ (e) => {
                              this.props.filterSchedule(e.target.value, this.props.classes)
                          } }>
                      <option value="">{lang ==='en' ? ('Trainers') : ('Մարզիչներ')}</option>
                      {this.props.trainer_options ? this.props.trainer_options.map((option) => (
                          <option value={option.id} key={option.id}>{option.name}</option>
                      )) :(null)}
                </select>
              </div>
            </div>
          </div>
        </div>
      </form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({filterSchedule}, dispatch);
};

const Filter = connect(state => ({
    classes:state.schedule.classes,
    trainers: state.schedule.trainers,
    class_options:state.schedule.class_options,
    trainer_options:state.schedule.trainer_options,
}), mapDispatchToProps)(FilterComponent);

export default Filter;