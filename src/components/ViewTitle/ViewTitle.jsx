import React, { Component } from 'react';
import './ViewTitle.css';

export default class ViewTitle extends Component {
  render() {
    return (
      <div className="view_title">
        <h1>
          {this.props.inner}
        </h1>
      </div>
    )
  }
}
