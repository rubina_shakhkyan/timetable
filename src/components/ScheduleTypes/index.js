import React, { Component } from 'react';
import './style.css';
import {loadSchedule} from "../../actions";

class ScheduleTypes extends Component {
    render() {
        if(this.props.types){
            let activeType = this.props.activeType;
            if(!this.props.activeType || this.props.activeType === 117){
                activeType = this.props.types[0].id;
            }
            return (
              <div className="row">
                  <div className="column small-12">
                      <ul className="nav nav-tabs">
                        {this.props.types.map((type) =>

                          <li className="nav-list" key={type.id}>
                              <button onClick={e => this.props.loadSchedule(type.id)}
                                      key={type.id}
                                      className={type.id === activeType ? ("nav-link active"):('nav-link')}
                              >
                                {type.name}
                              </button>
                          </li>

                        )
                        }
                      </ul>
                  </div>
              </div>
            );
        }else{
            return (null);
        }

    }
}

export default ScheduleTypes;
