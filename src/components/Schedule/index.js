import React, {Component} from 'react';
import './schedule.css'
import ScheduleRow from './../ScheduleRow';
import ScheduleTypes from '../../containers/ScheduleTypes';
import Time from "./../Time";
import CalendarUtil from "../../utils/CalendarUtil";
import Filter from "../Filter/Filter";
import { StickyContainer, Sticky } from 'react-sticky';
import StickySchedule from '../../containers/StickySchedule';

 class Schedule extends Component{
   componentDidMount(){
     this.props.loadScheduleTypes();
     this.props.loadSchedule(117);
   }

   render(){
     // console.log('times', this.props.data);
     return(
       <div>
         <div className="view_container no-spaces-top">
           <ScheduleTypes/>
           <Filter/>
           <div className="row">
             <div className="column small-12">
               <StickySchedule  />
               <div className="calendar calendar-card">
                 <StickyContainer className="main-schedule">
                 {

                 }

                   <Sticky>
                     {({
                         style,
                         isSticky,
                         wasSticky,
                         distanceFromTop,
                         distanceFromBottom,
                         calculatedHeight
                       }) => (
                       <div className="calendar-days" style={style}>
                         {this.props.weekdays.map((value, key) => (
                           <div key={key} >
                             <div className= {`main-day ${ CalendarUtil.isToday(value) ? ('active') : ('')}` }>
                               {value}
                             </div>
                           </div>
                         ))}
                       </div>
                     )}
                   </Sticky>
                   <div className="calendar-main">

                       {this.props.data ? (
                           this.props.data.map((val, key) => (
                               <table key={key}>
                                 <tbody>
                                   <tr>
                                       { this.props.times ? (
                                           <td className="time-td">
                                               <Time time={this.props.times[key]} />
                                           </td>
                                       ): (null)}
                                       <td>
                                           <ScheduleRow data={this.props.data[key]} switchStatus={this.props.switchStatus} index = {key} activeType={this.props.activeType} />
                                       </td>

                                   </tr>
                                 </tbody>


                               </table>
                           ))

                       ) : (null)}
                   </div>
                 </StickyContainer>
               </div>
             </div>
           </div>

         </div>
       </div>
     );
   }
 }



export default Schedule;
