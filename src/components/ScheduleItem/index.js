import React, {Component} from 'react';

export default class ScheduleItem extends Component {
  constructor(props){
      super(props);
      this.state = {
          tooltipVisible:false,
          data:this.props.data
      }
  }

    render() {
        const item = this.props.data;
        // console.log(item.id);
        let token = localStorage.getItem('token');
        return (

                      !item.hide ? (
                          <div  key={item.id} className="date-item-wrap">
                              <div  className={"date-item date-event " + (item.status ? '': 'signed') + (item.half ? ' ' : '') + (item.active == 0 ? ' white-bg' : '')} >
                                  {token && typeof(token!="undefined") ? (
                                      <button className="date-icon"
                                              onClick={e =>  this.props.switchStatus(item.id, this.props.activeType, item.status)}  disabled={(item.active != 1) }>
                                      <span className="icon icon-event">
                                      </span>
                                          <span className="date-icon--inner">
                                              {item.status ? '-' : '+'}
                                      </span>
                                      </button>
                                  ):(null)}

                                  <div>
                                      <div className="date-hours">
                                          {item.time }
                                      </div>
                                      <div className="date-type"> {item.title } </div>
                                      <div>
                                          { item.trainers ? (<div className="date-coach" >{item.trainers[0].name} </div>):(null)

                                          }
                                              </div>
                                      <div className="data-room"> {item.room} </div>
                                  </div>
                              </div>
                              {item.active!='1' ? (
                                  <div className="schedule_popup">
                                      <div className="schedule_popup_content">

                                          <div className="flex-container align-middle nodata_info">
                                              <div className="nodata_icon">
                                                  <svg xmlns="http://www.w3.org/2000/svg" width="31" height="31"><path data-name="Forma 1" d="M15.5 31A15.5 15.5 0 1 1 31 15.5 15.516 15.516 0 0 1 15.5 31zm0-29.23A13.73 13.73 0 1 0 29.228 15.5 13.745 13.745 0 0 0 15.5 1.77zm-4.321 18.94a.89.89 0 0 1-.626-1.52l5.323-5.32s.008-.01.012-.01l3.307-3.31a.9.9 0 0 1 1.253 0 .882.882 0 0 1 0 1.25l-5.323 5.33s-.008.01-.012.01l-3.306 3.31a.9.9 0 0 1-.628.26zm8.642 0a.9.9 0 0 1-.626-.26l-3.307-3.31s-.008-.01-.012-.01l-5.323-5.33a.882.882 0 0 1 0-1.25.9.9 0 0 1 1.253 0l3.306 3.31s.008.01.012.01l5.323 5.32a.89.89 0 0 1-.626 1.52z" fill="#868585" fillRule="evenodd"/></svg>
                                              </div>
                                              <div className="nodata_caption">
                                                  {localStorage.getItem('language') === 'en' ? 'The workout  will not take place' : 'Մարզումը տեղի չի ունենա'}
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              ):(
                                  null
                              )}
                          </div>
                          ) :(<NoDataItem  />)



        )
    }

}

export const NoDataItem = () => (
    <div className="date-item no-data-event">
        {/*no data*/}
    </div>
);
