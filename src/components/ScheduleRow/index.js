import React, { Component } from 'react';
import ScheduleItem, {NoDataItem} from './../ScheduleItem';
class ScheduleRow extends Component {
    render() {
        var data = new Array(7);
        var col_data = typeof this.props.data !== "undefined" ? this.props.data : [];
        col_data.forEach(function (key, value) {
            if(typeof data[parseInt(key.weekday-1)] === "undefined" ){
                data[parseInt(key.weekday-1)]= [key];
            }else{
                data[parseInt(key.weekday-1)]= data[parseInt(key.weekday-1)].concat(key);
            }

        });
        let fullInd = -1;
        var rows = [];
        for(var i = 0; i < 7; i++){
          if(data[i]){
            fullInd++;
          }
            rows.push(
                  data[i] ? (
                      <div className="day-main" key={i}>
                          {data[i].map((item, key)=><ScheduleItem key={key} data = {item} switchStatus={this.props.switchStatus} index ={[this.props.index, fullInd+key]} activeType={this.props.activeType}/>)}

                      </div>

                  ):(
                      <div className="day-main" key={i}>
                          <NoDataItem />
                      </div>

                  )

            );
          // console.log(rows);
        }
        return (
            <div className="date-wrapper">
                {rows}
            </div>
        );
    }
}

export default ScheduleRow;
