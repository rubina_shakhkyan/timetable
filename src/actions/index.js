import axios from 'axios';
const apiUrl = 'https://api.orangefitness.am/public';
let lang = localStorage.getItem('language') ? localStorage.getItem('language') : 'hy';
// localStorage.setItem('token', '46be3326247fa09f53f57482472e1e9c');
let token = localStorage.getItem('token');

export const loadScheduleSuccess = (data, id) => ({
    type: 'LOAD_SCHEDULE',
    data,
    id
});
export const loadSchedule = (id) => {
    localStorage.setItem('active',id);
    localStorage.removeItem('classes');
    localStorage.removeItem('trainers');
    if(token){
        return (dispatch) => {
            return axios.get(`${apiUrl}/api/classes/items/${id }/${lang}`, {headers:{ "authorizations":token}}
            )
                .then(response => {
                    dispatch(loadScheduleSuccess(Object.values(response.data), id))
                })
                .catch(error => {
                    throw(error);
                });
        };
    }else{
        return (dispatch) => {
            return axios.get(`${apiUrl}/api/classes/items/${id }/${lang}`, {}
            )
                .then(response => {
                    // console.log(response.data);
                    dispatch(loadScheduleSuccess(Object.values(response.data), id))
                })
                .catch(error => {
                    throw(error);
                });
        };
    }

};
export const loadScheduleTypesSuccess = (data)=>({
    type: 'LOAD_SCHEDULE_TYPES',
    data
});
export const loadScheduleTypes = () => {
    return (dispatch) => {
        return axios.get(`${apiUrl}/api/classesTimeLine/${lang}`, {}
        )
            .then(response => {
                dispatch(loadScheduleTypesSuccess(response.data))
            })
            .catch(error => {
                throw(error);
            });
    };
};


export const filterSchedule = (trainers, classes) => ({
   type: 'FILTER_SCHEDULE',
   trainers,
   classes
});
export const switchStatusSuccess = (index) => {
    return{
        type: 'SWITCH_STATUS',
        index
    }
};

export const switchStatus = (schedule_id, group_id, status)  => {
    let data = new FormData();
    data.append('group_id',group_id);
    data.append('schedule_id',schedule_id);
    if(status){
        return (dispatch) => {
            return axios.post(`${apiUrl}/api/deleteTimeTable/${lang}`, data, {headers:{ "authorizations":token}}
            )
                .then(response => {
                    dispatch(loadSchedule(group_id, token))
                })
                .catch(error => {
                    throw(error);
                });
        };
    }else{
        return (dispatch) => {
            return axios.post(`${apiUrl}/api/bookmarkTimeTable/${lang}`, data, {headers:{ "authorizations":token}}
            )
                .then(response => {
                    dispatch(loadSchedule(group_id, token))
                })
                .catch(error => {
                    throw(error);
                });
        };
    }

}
