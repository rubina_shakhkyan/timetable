import { connect } from 'react-redux';
import CalendarUtil from "../../utils/CalendarUtil";
import { bindActionCreators } from 'redux';
import ScheduleComponent from '../../components/Schedule';
import { loadSchedule, switchStatus, loadScheduleTypes} from '../../actions';

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({loadSchedule,switchStatus, loadScheduleTypes}, dispatch);
};

const Schedule = connect(state => ({
  data : state.schedule.data,
  activeType: state.schedule.activeType,
  weekdays: CalendarUtil.getWeekDays(),
  times: state.schedule.times,
}), mapDispatchToProps)(ScheduleComponent);

export default Schedule;
