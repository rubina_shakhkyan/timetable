import { connect } from 'react-redux';
import ScheduleItemComponent from '../../components/ScheduleItem';
import { bindActionCreators } from 'redux';
import { switchStatus } from '../../actions/schedule'

const mapStateToProps = (state) => {
  return {
    data: props.data
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({switchStatus}, dispatch);
}

const ScheduleItem = connect(mapStateToProps, mapDispatchToProps)(ScheduleItemComponent);
