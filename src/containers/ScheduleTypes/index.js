import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ScheduleTypesComponent from '../../components/ScheduleTypes';
import { loadSchedule } from '../../actions';

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loadSchedule}, dispatch);
}

const ScheduleTypes = connect(state => ({
    types : state.schedule.types,
    activeType: state.schedule.activeType,
}), mapDispatchToProps)(ScheduleTypesComponent)

export default ScheduleTypes;
