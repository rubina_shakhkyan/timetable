import { connect } from 'react-redux';
import CalendarUtil from "../../utils/CalendarUtil";
import { bindActionCreators } from 'redux';
import StickyScheduleComponent from '../../components/StickySchedule/StickySchedule';
import { loadSchedule, switchStatus, loadScheduleTypes} from '../../actions';

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loadSchedule,switchStatus, loadScheduleTypes}, dispatch);
};

const StickySchedule = connect(state => ({
    data : state.schedule.data,
    activeType: state.schedule.activeType,
    weekdays: CalendarUtil.getWeekDays(),
    times: state.schedule.times,
}), mapDispatchToProps)(StickyScheduleComponent);

export default StickySchedule;
